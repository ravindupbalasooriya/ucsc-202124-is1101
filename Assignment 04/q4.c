#include<stdio.h>

int main()
{
    
    int  num;
    int i;
    printf("Enter a number to find factors : ");
    scanf("%d",&num);
    printf("Factors of %d are\n", num);

    for(i = 1; i <= num/2; i++)
    {
        if(num%i == 0)
            printf("%d\n", i);
    }

    return 0;
}