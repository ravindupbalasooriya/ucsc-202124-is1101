#include <stdio.h>
int main() {
    int num;
    int rev = 0;
    int rem;

    printf("Enter an integer to reverse: ");
    scanf("%d", &num);
    
    while (num != 0) {
        rem = num % 10;
        rev = rev * 10 + rem;
        num /= 10;
    }
    printf("The Reversed number = %d", rev);
    return 0;
}

