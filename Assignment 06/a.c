#include <stdio.h>

void pattern(int i); 
void Row(int j); 
int NoOfRows=1; 
int r;

int main(){
    printf("How many rows? : ");
    scanf("%d",&r);
    pattern(r);
    return 0;
}


void pattern(int i){  
    
    if(i>0){
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(i-1);
        
    }
}


void Row(int j){
    if(j>0){
        printf("%d",j);
        Row(j-1);
    }
}

