#include <stdio.h>
#include <conio.h>
#include <string.h>

int main(){


    //WRITING THE 1ST TEXT TO THE DOCUMENT///////////////////////////////////////////////////////

    FILE *Fp;
    char text[200] = "UCSC is one of the leading institutes in Sri Lanka for computing studies.";
    int length = strlen(text);
    int n;



    Fp=fopen("assignment9.txt", "w");

    if (Fp==NULL)
    {
        printf("File creation failed");
    }else{
        for (n = 0; n < length; n++)
        {
            printf("Writing the character :  %c \n", text[n]);
                fputc(text[n],Fp);
        }
        
        printf("\n");
        printf("All the characters written successfully\n\n");
        fclose(Fp);
    }


    //READING THE 1ST TEXT IN THE DOCUMENT///////////////////////////////////////////////////////

    printf("\n\n");
    printf("***Reading the final document***\n\n");

    Fp=fopen("assignment9.txt", "r");

    fscanf(Fp, "%[^\n]s", text);
    printf("%s", text);
    fclose(Fp);

    printf("\n\n");
    printf("***Reading the final document successfull.\n\n");



    //APPENDING THE 2ND TEXT IN THE DOCUMENT///////////////////////////////////////////////////////

    printf("\n\n");
    printf("***Appending new characters***\n\n");
    
    char text2[200] = "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.";
    int length2 = strlen(text2);
    int n2;

    Fp=fopen("assignment9.txt", "a");

    if (Fp==NULL)
    {
        printf("File appending failed");
    }else{
        for (n2 = 0; n2 < length2; n2++)
        {
            printf("Writing the character :  %c \n", text2[n2]);
                fputc(text2[n2],Fp);
        }
        
        printf("\n");
        printf("***All the characters appended successfully***\n\n");
        fclose(Fp);
    }


    //READING THE FINAL TEXT IN THE DOCUMENT///////////////////////////////////////////////////////


    printf("\n");
    printf("***Reading the final document***\n\n");

    Fp=fopen("assignment9.txt", "r");

    fscanf(Fp, "%[^\n]s", text2);
    printf("%s", text2);
    fclose(Fp);

    printf("\n\n");
    printf("***Reading the final document successfull.Press any key to exit***\n\n");




    getch();
    return 0;

}