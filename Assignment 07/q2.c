#include<stdio.h>
int main()
{
    int i, count=0;
    char str[100], testchar;
    printf("Enter the String: ");
    gets(str);
    printf("Enter character to find frequency: ");
    scanf("%c", &testchar);
    for(i=0; str[i]!='\0'; i++){
        if(testchar==str[i])
            count++;
    }
    printf("\nFrequency of %c = %d", testchar, count);

    return 0;
}
